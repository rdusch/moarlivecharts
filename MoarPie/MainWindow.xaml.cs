﻿namespace MoarPie
{
    using LiveCharts.Defaults;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.ComponentModel;
    using System.Timers;
    using System.Windows;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Timer updateTimer = new Timer(TimeSpan.FromSeconds(5).TotalMilliseconds);
        private Random Randy = new Random();
        private ObservableValue Ovalue1 = new ObservableValue(1);
        private ObservableValue Ovalue2 = new ObservableValue(1);
        private ObservableValue Ovalue3 = new ObservableValue(2);
        private ObservableValue Ovalue4 = new ObservableValue(3);
        private ChartValueExt Value1 = new ChartValueExt() { Title = "Value1" };
        private ChartValueExt Value2 = new ChartValueExt() { Title = "Value2" };
        private ChartValueExt Value3 = new ChartValueExt() { Title = "Value3" };
        private ChartValueExt Value4 = new ChartValueExt() { Title = "Value4" };

        public MainWindow()
        {
            this.Value1.Add(Ovalue1);
            this.Value2.Add(Ovalue2);
            this.Value3.Add(Ovalue3);
            this.Value4.Add(Ovalue4);

            this.Values = new List<ChartValueExt>(){
                this.Value1,
                this.Value2,
                this.Value3,
                this.Value4
            };

            InitializeComponent();

            this.updateTimer.Elapsed += updateTimer_Elapsed;
            this.updateTimer.Start();

            DataContext = this;
        }

        public List<ChartValueExt> Values { get; set; }
        
        private double _innerRadius;

        public double InnerRadius
        {
            get { return _innerRadius; }
            set
            {
                if (_innerRadius != value)
                {
                    _innerRadius = value;
                    this.OnPropertyChanged();
                }
            }
        }


        private void updateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Ovalue1.Value = Randy.Next(5);
            this.Ovalue2.Value = Randy.Next(5);
            this.Ovalue3.Value = Randy.Next(5);
            this.Ovalue4.Value = Randy.Next(5);
            this.InnerRadius = Randy.Next(5) * 4;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
