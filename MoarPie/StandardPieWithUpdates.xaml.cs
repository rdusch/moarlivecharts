﻿namespace MoarPie
{
    using LiveCharts;
    using LiveCharts.Defaults;
    using LiveCharts.Wpf;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for StandardPieWithUpdates.xaml
    /// </summary>
    public partial class StandardPieWithUpdates : UserControl
    {
        public StandardPieWithUpdates()
        {
            InitializeComponent();
            this.PART_PieChart.Series = new SeriesCollection();
        }

        public double InnerRadius
        {
            get { return (double)GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InnerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadius", typeof(double), typeof(StandardPieWithUpdates), new FrameworkPropertyMetadata(InnerRadiusChangedCallBack));

        private static void InnerRadiusChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardPieWithUpdates pieChart = (StandardPieWithUpdates)d;
            pieChart.UpdateInnerRadius(e.NewValue as double?);
        }

        private void UpdateInnerRadius(double? innerRadius)
        {
            if (innerRadius.HasValue)
            {
                this.PART_PieChart.InnerRadius = innerRadius.Value;
            }
        }

        public List<ChartValueExt> Values
        {
            get { return (List<ChartValueExt>)GetValue(ValuesProperty); }
            set { SetValue(ValuesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Values.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValuesProperty =
            DependencyProperty.Register("Values", typeof(List<ChartValueExt>), typeof(StandardPieWithUpdates), new FrameworkPropertyMetadata(ValuesChangedCallBack));

        private static void ValuesChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardPieWithUpdates pieChart = (StandardPieWithUpdates)d;
            pieChart.SetPieValues(e.NewValue as List<ChartValueExt>);
        }

        private void SetPieValues(List<ChartValueExt> list)
        {
            foreach (ChartValueExt cvov in list)
            {
                this.PART_PieChart.Series.Add(new PieSeries() { Values = cvov, Title = cvov.Title });
            }
        }
    }
}
