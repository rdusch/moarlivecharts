﻿using LiveCharts;
using LiveCharts.Defaults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoarPie
{
    public class ChartValueExt : ChartValues<ObservableValue>
    {
        public string Title { get; set; }
    }
}
