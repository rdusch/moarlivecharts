﻿namespace MoarPie
{
    using LiveCharts;
    using LiveCharts.Wpf;
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for PercentageChartWithUpdates.xaml
    /// </summary>
    public partial class PercentageChartWithUpdates : UserControl
    {
        public PercentageChartWithUpdates()
        {
            Formatter = val => val.ToString("P");
            InitializeComponent();
            this.PART_PercentageChart.Series = new SeriesCollection();
        }

        public Func<double, string> Formatter { get; set; }

        public List<ChartValueExt> Values
        {
            get { return (List<ChartValueExt>)GetValue(ValuesProperty); }
            set { SetValue(ValuesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Values.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValuesProperty =
            DependencyProperty.Register("Values", typeof(List<ChartValueExt>), typeof(PercentageChartWithUpdates), new FrameworkPropertyMetadata(ValuesChangedCallBack));

        private static void ValuesChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PercentageChartWithUpdates percentageChart = (PercentageChartWithUpdates)d;
            percentageChart.SetValues(e.NewValue as List<ChartValueExt>);
        }

        private void SetValues(List<ChartValueExt> list)
        {
            foreach (ChartValueExt cvov in list)
            {
                this.PART_PercentageChart.Series.Add(new StackedRowSeries() { Values = cvov, LabelPoint = p => cvov.Title });
            }
        }
    }
}
